import { createSubscriptionForm } from "./subscription-form.js";

const JoinUsSection = (function () {
  function createSection() {
    const section = document.createElement("section");
    section.className = "app-section app-section--image-join";

    // Create a <h2> element for the section title, set its class, and text content.
    const h2 = document.createElement("h2");
    h2.className = "app-title";
    h2.textContent = "Join Our Program";

    // Create a <p> element for the section description, set its class, and text content.
    const p = document.createElement("p");
    p.className = "app-section__paragraph";
    p.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

    //Append the title (h2) and description (p) to the section element.
    section.appendChild(h2);
    section.appendChild(p);

    // Append the form to the section element.
    const form = createSubscriptionForm();
    section.appendChild(form);

    return section;
  }

  // Private function to insert the created section before the footer element on the page.
  function insertBeforeFooter() {
    const section = createSection(); // Create the section element.
    const footer = document.querySelector(".app-footer"); // Find the footer element in the DOM.
    footer.parentNode.insertBefore(section, footer); // Insert the section before the footer in the DOM.
  }

  return {
    init: insertBeforeFooter,
  };
})();

export { JoinUsSection };
