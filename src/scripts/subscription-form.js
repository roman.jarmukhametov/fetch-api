// Imports the email validation function from the email-validator module.
import { validate } from "./email-validator.js";

// Imports utility functions for managing subscription status in local storage.
import {
  getSubscriptionStatus,
  setSubscriptionStatus,
  removeSubscriptionStatus,
} from "./local-storage-util.js";

/**
 * Creates and returns a subscription form element with email input and submit button.
 * It also sets up the form's event listener for the submit event.
 *
 * @returns {HTMLElement} The subscription form element.
 */
function createSubscriptionForm() {
  // Creates the form element and sets its class and action attributes.
  const form = document.createElement("form");
  form.className = "app-section__form";
  form.action = "#";

  // Creates the email input field, setting its type, placeholder, and class.
  const emailInput = document.createElement("input");
  emailInput.type = "email";
  emailInput.placeholder = "Email";
  emailInput.className = "app-section__input--email";

  // Creates the submit button, setting its class, type, and value attributes.
  const submitButton = document.createElement("input");
  submitButton.className = "app-section__button app-section__button--subscribe";
  submitButton.type = "submit";
  submitButton.value = "Subscribe";

  // Appends the email input and submit button to the form element.
  form.appendChild(emailInput);
  form.appendChild(submitButton);

  // Attaches an event listener to handle form submission.
  form.addEventListener("submit", (event) => {
    event.preventDefault(); // Prevents the default form submission.
    handleFormSubmit(emailInput, submitButton);
  });

  // Adjusts the form UI based on the current subscription status.
  adjustFormUI(form, emailInput, submitButton);

  return form; // Returns the fully constructed form element.
}

/**
 * Handles the form submission event. Validates the email and updates the UI and local storage based on the validation result.
 *
 * @param {HTMLInputElement} emailInput - The email input element of the form.
 * @param {HTMLInputElement} submitButton - The submit button element of the form.
 */
function handleFormSubmit(emailInput, submitButton) {
  // Validates the entered email address.
  const isValidEmail = validate(emailInput.value);
  // Retrieves the current subscription status from local storage.
  const isSubscribed = getSubscriptionStatus();

  // Attempts to remove any existing error message from previous submissions.
  const existingErrorMsg = document.querySelector(".error-message");
  if (existingErrorMsg) {
    existingErrorMsg.remove();
  }

  // Checks if the email is valid and the user is not already subscribed.
  if (isValidEmail && !isSubscribed) {
    setSubscriptionStatus(true, emailInput.value); // Updates the subscription status in local storage.
    // Additional UI adjustments for a subscribed state are handled here...
  } else if (isSubscribed) {
    removeSubscriptionStatus(); // Removes the subscription status from local storage.
    window.location.reload(); // Reloads the page to reset the form state.
  } else {
    // Handles the case of an invalid email address.
    emailInput.className = "error-message"; // Temporarily repurposes the email input for displaying an error message.
  }
}

/**
 * Adjusts the form's UI based on the subscription status, hiding the email input and changing the submit button text if subscribed.
 *
 * @param {HTMLFormElement} form - The form element containing the email input and submit button.
 * @param {HTMLInputElement} emailInput - The email input element of the form.
 * @param {HTMLInputElement} submitButton - The submit button element of the form.
 */
function adjustFormUI(form, emailInput, submitButton) {
  // Checks if the user is currently subscribed.
  const isSubscribed = getSubscriptionStatus();
  if (isSubscribed) {
    emailInput.style.display = "none"; // Hides the email input field.
    submitButton.value = "Unsubscribe"; // Changes the submit button text to indicate an unsubscribe action.
    form.style.justifyContent = "center"; // Centers the unsubscribe button in the form.
  }
}

// Exports the createSubscriptionForm function to make it available for import in other modules.
export { createSubscriptionForm };
